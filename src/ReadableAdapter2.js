const { Readable } = require('stream')

const Debug = require('debug')
const debug = Debug('ReadableAdapter2')

let ArrayQueue = require('real-value-arrayqueue')


const ReadableAdapter2 = (options = {}) => {
    let { name = '',
            eventHandler,
            highWaterMark = 10,
            delay = 250,
            allProcessed = () => true,
            done = () => {},
            maxListeners = 10,
            queueStorage = ArrayQueue({ name })
         } = options
    let hasReader = false
    let allRead = false

    function producer (x) {
        if (x === null) { allRead = true } else { queueStorage.enqueue(x) }
    }

    // This applies the back pressure where be slow down a READER to match a WRITER
    let checkMark = (readable, delay, highWaterMark, callback) => {
        let mark = queueStorage.length()
        if (mark >= highWaterMark) {
            setTimeout(checkMark, delay, readable, delay, highWaterMark, callback)
        } else {
            callback()
        }
    }

    let pushChunks = (readable, delay, numberAllowedToPush) => {
        if (allRead && queueStorage.length() === 0 && allProcessed()) {
            debug(`${name} signaling stream end`)
            readable.push(null)
            done()
            return
        }
        if (queueStorage.length() > 0) {
            let count = numberAllowedToPush
            debug(`Count ${count}`)
            while (queueStorage.length() > 0 && count > 0) {
                let next = queueStorage.dequeue()
                debug(`${name}: Writing  Value:${next} : RemainingDepth:${depth()}`)
                let p = readable.push(next)
                count--
                if (!p) {
                    return
                }
            }
        } else {
            setTimeout(pushChunks, delay, readable, delay, numberAllowedToPush)
        }
    }

    let depth = () => {
        return queueStorage.length()
    }

    let readable = new Readable({
        highWaterMark,
        objectMode: true,

        /* Read is called when this node is allowed to push content to downstream node */
        read (n) {
                hasReader = true
                debug(`${name} read canberead:${n}`)
                pushChunks(this, delay, n)
        }
    })
    readable.setMaxListeners(maxListeners)
    return {
        readable,
        producer
    }
}

const ReadableAdapterQueueStorage = ReadableAdapter2

module.exports = {
    ReadableAdapter2,
    ReadableAdapterQueueStorage
}
