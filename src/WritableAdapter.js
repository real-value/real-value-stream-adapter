const { Writable } = require('stream')

const debug = require('debug')('WritableAdapter')

const WritableAdapter = (options) => {
    let {
        name = '',
        add,
        done,
        highWaterMark = 256,
        delay = 0,
        finalCallback = () => {}
    } = options

    let setFinalCallback = (f) => {
        finalCallback = f
    }

    let writable = new Writable({
        highWaterMark,
        objectMode: true,
        write (chunk, encoding, callback) {
            debug('Add')
            let mark = add(chunk)
            if (delay > 0 && mark >= highWaterMark) {
                debug('Delay')
                setTimeout(() => {
                    callback()
                }, delay)
            } else {
                callback()
            }
        },
        writev: async function (chunks, callback) {
            for (var i = 0; i < chunks.length; i++) {
                debug('Add')
                let mark = add(chunks[i].chunk)
                if (mark >= highWaterMark) {
                    debug('Delay')
                    await new Promise((resolve) => { setTimeout(() => { chunks[i].callback(); resolve() }, delay) })
                } else {
                    chunks[i].callback()
                }
            }
            callback()
        },
        /* Final is called when all upstream content is pushed to the node */
        final (callback) {
            debug(`${name} final`)
            done()
            callback()
            finalCallback((x) => add(x))
        }
    })

    return {
        setFinalCallback,
        writable
    }
}

module.exports = {
    WritableAdapter
}
