const { Duplex } = require('stream')

const Debug = require('debug')
const debug = Debug('DuplexAdapter')
const debugIO = Debug('DuplexAdapterIO')

let ArrayQueue = require('real-value-arrayqueue')

const DuplexAdapter = (options) => {
    let { name = '',
            eventHandler,
            done =()=>{},
            highWaterMark = 10,
            delay = 250,
            allProcessed = () => true,
            finalCallback = () => {},
            destroyCallback = () => {},
            maxListeners = 10,
            queueStorage = ArrayQueue({ name })
         } = options
    let hasReader = false
    let allRead = false
    let add = (x) => {
        queueStorage.enqueue(x)
    }

    let setFinalCallback = (f) => {
        finalCallback = f
    }
    let setDestroyCallback = (f) => {
        destroyCallback = f
    }

    // This applies the back pressure where be slow down a READER to match a WRITER
    let checkMark = (duplex, delay, highWaterMark, callback) => {
        let mark = queueStorage.length()
        if (mark >= highWaterMark) {
            setTimeout(checkMark, delay, duplex, delay, highWaterMark, callback)
        } else {
            callback()
        }
    }

    let pushChunks = (duplex, delay, numberAllowedToPush) => {
        if (allRead && queueStorage.length() === 0 && allProcessed()) {
            debug(`${name} signaling stream end`)
            duplex.push(null)
            return
        }
        if (queueStorage.length() > 0) {
            let count = numberAllowedToPush
            debug(`Count ${count}`)
            while (queueStorage.length() > 0 && count > 0) {
                let next = queueStorage.dequeue()
                debugIO(`${name}: Writing  Value:${next} : RemainingDepth:${depth()}`)
                let p = duplex.push(next)
                count--
                if (!p) {
                    return
                }
            }
        } else {
            setTimeout(pushChunks, delay, duplex, delay, numberAllowedToPush)
        }
    }

    let depth = () => {
        return queueStorage.length()
    }

    let duplex = new Duplex({
        emitClose: true,
        highWaterMark,
        objectMode: true,

        /* Read is called when this node is allowed to push content to downstream node */
        read (n) {
                hasReader = true
                debug(`${name} read canberead:${n}`)
                pushChunks(this, delay, n)
        },

        /* Write is called when the upstream pushes content to this node */
        write (chunk, encoding, callback) {
            debugIO(`${name}: Received Value:${chunk} : PrecedingDepth:${depth()}`)
            eventHandler(chunk, depth, (x) => add(x), () => {
                hasReader
                    ? checkMark(this, delay, highWaterMark, callback)
                    : callback()
            })
        },

        /* Final is called when all upstream content is pushed to the node */
        final (callback) {
            debug(`${name} final`)
            allRead = true
            done()
            finalCallback((x) => add(x),callback)
        },

        /* Destroy is called when all downstream content is written from the node (when the node pushes null) */
        destroy: destroyCallback,

        autoDestroy: true // This enables the destroy callback

        // writev: async function (chunks, callback) {
        //     for (var i = 0; i < chunks.length; i++) {
        //         //debug('write2')
        //         let chunk = chunks[i].chunk
        //         eventHandler(chunk,(x)=>add(x),()=>{
        //             checkMark(this, delay, highWaterMark, chunks[i].callback())
        //         })
        //     }
        //     callback()
        // }
    })
    duplex.setMaxListeners(maxListeners)
    return {
        duplex,
        setFinalCallback,
        setDestroyCallback
    }
}

module.exports = {
    DuplexAdapter
}
