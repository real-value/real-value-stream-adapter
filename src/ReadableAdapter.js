const { Readable } = require('stream')

const debug = require('debug')('ReadableAdapter')

const ReadableAdapter = (iterator) => {
    // This is not available in browser as yet
    // return Readable.from(iterator())

    return new Readable({
        objectMode: true,
        async read (size) {
            debug('Read')
            let shouldContinue = true
            while (shouldContinue) {
                let { done, value } = await iterator.next()
                if (done) {
                    debug('Read done')
                    this.push(null)
                    break
                } else {
                    shouldContinue = this.push(value)
                    if (!shouldContinue) { break }
                }
            }
        }
    })
}

module.exports = {
    ReadableAdapter
}
