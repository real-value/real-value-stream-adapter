
let { WritableAdapter } = require('./src/WritableAdapter.js')
let { ReadableAdapter } = require('./src/ReadableAdapter.js')
let { ReadableAdapter2 } = require('./src/ReadableAdapter2.js')
let { DuplexAdapter } = require('./src/DuplexAdapter.js')
module.exports = { WritableAdapter, ReadableAdapter,ReadableAdapter2,DuplexAdapter }