const debug = require('debug')('transform')
const assert = require('assert')

const { Duplex } = require('stream')
const bufferFrom = require('buffer-from')

const csvTransform = (options = { newline: bufferFrom('\n')[0] }) => {
  let _prev = null
  let previousEnd = 0
  let start = 0

  let headers = null

  let running = true

  function parseLine (tx, buffer, start, end) {
    let row = buffer.toString('utf-8', start, end).trim()
    if (!headers) {
      headers = row.split(',')
    } else {
      let o = {}
      let values = row.split(',')
      for (let i = 0; i < values.length; i++) {
          o[headers[i]] = values[i]
      }
      debug(o)
      let r = tx.push(JSON.stringify(o))
      if (!r) {
          debug('halt running')
          running = false
      }
    }
  }

  return new Duplex({
    // highWaterMark: 1024,
    read (size) {
      debug('running')
      running = true
    },
    write: async function (buffer, encoding, cb) {
      if (_prev) {
        buffer = Buffer.concat([_prev, buffer])
        _prev = null
      }

      const bufferLength = buffer.length

      for (let i = start; i < bufferLength; i++) {
        const chr = buffer[i]
        const nextChr = i + 1 < bufferLength ? buffer[i + 1] : null

        if (chr === options.newline) {
          parseLine(this, buffer, previousEnd, i + 1)
          previousEnd = i + 1
        }

        // while (!running) {
        //   debug('waiting')
        //   await new Promise((resolve) => { setTimeout(() => { resolve() }, 100) })
        // }
      }

      if (previousEnd === bufferLength) {
        previousEnd = 0
        return cb()
      }

      if (bufferLength > previousEnd) {
        _prev = Buffer.from(buffer).slice(previousEnd)
        previousEnd = 0
        return cb()
      }
    }
  })
}

module.exports = {
  csvTransform
}
