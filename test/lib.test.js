import fs from 'fs'
import { describe } from 'riteway'
import { ReadableAdapter, ReadableAdapter2, WritableAdapter, DuplexAdapter } from '../index.js'

const debug = require('debug')('test')
const debugDetailed = require('debug')('testDetailed')
const csv = require('csv-parser')

const ArrayQueue = require('real-value-arrayqueue')

describe('Writable Simple CSV', async (assert) => {
  let count = 0
  let add = (x) => {
    debug(x)
    count++
    return count
  }
  let done = () => {
    debug('done')
    assert({
      given: 'a pipe from a csv with 4 rows',
      should: 'be provided 4 rows',
      actual: count,
      expected: 4
    })
  }

  let writableadapter = WritableAdapter({
    add,
    done,
    highWaterMark: 2,
    delay: 100
  })
  writableadapter.setFinalCallback(() => console.log('here'))
  fs.createReadStream('test/data.csv')
  .pipe(csv())
  .pipe(writableadapter.writable)
})

describe('Readable from Generator', async (assert) => {
  let generator = function * () {
    for (var i = 0; i < 10; i++) {
      yield i
    }
  }
  let count = 0
  let add = (x) => {
    // debug(x)
    count++
    return x
  }
  let done = () => {
    assert({
      given: 'a pipe from generator',
      should: 'be provided 10 events',
      actual: count,
      expected: 10
    })
  }
  let g = generator()
  ReadableAdapter(g).pipe(WritableAdapter({
    add,
    done,
    highWaterMark: 5,
    delay: 100
  }).writable)
})

describe('Readable from Function', async (assert) => {
  let count = 0
  let add = (x) => {
    // debug(x)
    count++
    return x
  }
  let done = () => {
    assert({
      given: 'a pipe from generator',
      should: 'be provided 10 events',
      actual: count,
      expected: 2
    })
  }
  let { producer, readable } = ReadableAdapter2()
  readable.pipe(WritableAdapter({
    add,
    done,
    highWaterMark: 5,
    delay: 100
  }).writable)

  producer(1)
  producer(2)
  producer(null)
})

describe('Tee', async (assert) => {
  let generator = function * () {
    for (var i = 0; i < 10; i++) {
      yield i
    }
  }
  let count = 0
  let add = (x) => {
    debug(`Stream1: ${x}`)
    count++
    return x
  }
  let done = () => {
    assert({
      given: 'a pipe from generator',
      should: 'be provided 10 events',
      actual: count,
      expected: 10
    })
  }
  let r = ReadableAdapter(generator())
  r.pause()

  r.pipe(WritableAdapter({
    add,
    done,
    highWaterMark: 5,
    delay: 100
  }).writable)

  // await new Promise((resolve) => { setTimeout(() => { chunks[i].callback(); resolve() }, 100) })

  let add2 = (x) => {
    debug(`Stream2: ${x}`)
    return 10 - x
  }
  let done2 = () => {
    debug('Done2')
  }
    r.pipe(WritableAdapter({
      add: add2,
      done: done2,
      highWaterMark: 3,
      delay: 1000
    }).writable)
})

/*
 The purpose of this test is to check that destroy is only called if the the final callback is called .
 The final callback is a good place to tidy and flush any wip but the callback it is passed need to be called.
*/
describe('Duplex Check Final', async (assert) => {

  let generator = function * () {
    for (var i = 0; i < 3; i++) {
      yield i
    }
    debug('Generator done')
  }

  let destroyCalled = false
  let node1 = DuplexAdapter({
    name: 'node1',
    eventHandler: async (x, depth, downstream, callback) => {
        downstream(x)
        callback()
    },
    done: ()=>{},
    destroyCallback: ()=>{
      destroyCalled = true
    },
    finalCallback: (downstream,callback)=>{ 
      callback() 
    },
  })

  let node2 = DuplexAdapter({
    name: 'node2',
    eventHandler: async (x, depth, downstream, callback) => {
        callback()
    },
    finalCallback: (downstream,callback)=>{ 
      assert({
        given: `a finalCallback which calls the callback`,
        should: 'onFinish event be run',
        actual: destroyCalled,
        expected: true
      })
    }
  })

  ReadableAdapter(generator()).pipe(node1.duplex).pipe(node2.duplex)
    
})

describe('Duplex from Generator', async (assert) => {
  let PRODUCED_EVENTS = 10
  let generator = function * () {
    for (var i = 0; i < PRODUCED_EVENTS; i++) {
      yield i
    }
    debug('Generator done')
  }

  let countedQueueStorage = options => {
    let arrayQueue = ArrayQueue(options)
    let dequeueCount = 0
    let enqueueCount = 0
    return {
      getEnqueueCount: () => enqueueCount,
      getDequeueCount: () => dequeueCount,
      enqueue: x => {
        enqueueCount++
        // debug(`Enqueue ${options.name} ${enqueueCount}`)
        arrayQueue.enqueue(x)
      },
      length: () => arrayQueue.length(),
      dequeue: () => {
        dequeueCount++
        // debug(`Dequeue ${options.name} ${dequeueCount}`)
        return arrayQueue.dequeue()
      }
    }
  }

  let makeNode = (name, eventFn, expectedNumberOfEvents, expectedDequeue) => {
    let countEvents = 0
    let nodeComplete = false
    let nodeDelay = 10
    let highWaterMark = 5
    let eventHandlerFactory = (name, delay, processor) => (x, depth, downstream, callback) => {
      debug(`${name}: Received Value:${x} : PrecedingDepth:${depth()}`)
      countEvents++
      setTimeout(() => {
        let nx = processor(x)
        for (var i = 0; i < nx.length; i++) {
          downstream(nx[i])
        }
        callback()
      }, delay)
    }
    let doneFn = () => {
      debugDetailed(`upstream of ${name} done`)
      setTimeout(() => { nodeComplete = true }, nodeDelay) // Delay done for 1 sec
    }

    let countedQueue = countedQueueStorage({ name })

    let verification = () => {
      assert({
        given: `a stream node ${name}`,
        should: 'receive all produced events',
        actual: countEvents,
        expected: expectedNumberOfEvents
      })
      assert({
        given: `a stream node ${name}`,
        should: 'dequeue appropriate number of events',
        actual: countedQueue.getDequeueCount(),
        expected: expectedDequeue
      })
    }
    return {
      name,
      queueStorage: countedQueue,
      maxListeners: 10,
      eventHandler: eventHandlerFactory(name, nodeDelay, eventFn),
      done: doneFn,
      allProcessed: () => {
        debugDetailed(`all processed1 ${nodeComplete}`)
        return nodeComplete
      },
      finalCallback: (downstream,callback) => {
        debugDetailed(`${name} FinalCallback`)
        verification()
        callback()
      },
      destroyCallback: () => {
        debugDetailed(`${name} DestroyCallback`)
      },
      highWaterMark
    }
  }

  // x => [x]
  // x => [x, 10 * x]
  // x => x % 5 === 0 ? [x] : []

  let node1 = DuplexAdapter(makeNode('node1', x => [x], PRODUCED_EVENTS * 1, PRODUCED_EVENTS * 1)).duplex
  let node2 = DuplexAdapter(makeNode('node2', x => [x], PRODUCED_EVENTS * 1, 0)).duplex
  // let node3 = DuplexAdapter(makeNode('node3', x => [x], PRODUCED_EVENTS * 1, PRODUCED_EVENTS * 1))
  // let node4 = DuplexAdapter(makeNode('node4', x => [x], PRODUCED_EVENTS * 1, 0))

  let inputStream = ReadableAdapter(generator()).pipe(node1)
  inputStream.pipe(node2)
  // inputStream.pipe(node3).pipe(node4)
})

describe('Stream Merge', async (assert) => {
  let PRODUCED_EVENTS = 5
  let generator = (prefix) => function * () {
    for (var i = 0; i < PRODUCED_EVENTS; i++) {
      yield `${prefix}-${i}`
    }
  }

  let makeNode = (name, eventFn, expectedNumberOfEvents) => {
    let countEvents = 0
    let nodeComplete = false
    let nodeDelay = 10
    let highWaterMark = 5
    let eventHandlerFactory = (name, delay, processor) => (x, depth, downstream, callback) => {
      debug(`${name}: Received Value:${x} : PrecedingDepth:${depth()}`)
      countEvents++
      setTimeout(() => {
        let nx = processor(x)
        for (var i = 0; i < nx.length; i++) {
          downstream(nx[i])
        }
        callback()
      }, delay)
    }
    let doneFn = () => {
      debugDetailed(`upstream of ${name} done`)
      setTimeout(() => { nodeComplete = true }, nodeDelay) // Delay done for 1 sec
    }
    let verification = () => {
      assert({
        given: 'a stream node',
        should: 'receive all produced events',
        actual: countEvents,
        expected: expectedNumberOfEvents
      })
    }
    return {
      name,
      eventHandler: eventHandlerFactory(name, nodeDelay, eventFn),
      done: doneFn,
      allProcessed: () => {
        debugDetailed(`all processed1 ${nodeComplete}`)
        return nodeComplete
      },
      finalCallback: () => {
        debugDetailed(`${name} FinalCallback`)
        //
      },
      destroyCallback: () => {
        debugDetailed(`${name} DestroyCallback`)
        // verification()
      },
      highWaterMark
    }
  }

  // x => [x]
  // x => [x, 10 * x]
  // x => x % 5 === 0 ? [x] : []
  let node1 = DuplexAdapter(makeNode('node1', x => [x], PRODUCED_EVENTS * 1)).duplex

  let inputStream = ReadableAdapter(generator('A')())
  let inputStream2 = ReadableAdapter(generator('B')())

  let join1 = WritableAdapter({ add: x => console.log(x), done: () => console.log('Done') }).writable
  let join2 = WritableAdapter({ add: x => console.log(x), done: () => console.log('Done') }).writable

  inputStream.pipe(join1)
  inputStream2.pipe(join2)

  // await new Promise((resolve) => { setTimeout(() => { resolve() }, 5000) })
})
